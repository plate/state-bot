# state-bot
a bot mainly focused on stuff to do with your room's state events.
think of things that are tedious to find out by hand.

commands:

- `!firstjoin <user> <room>` - when a user first joined the room and some avatars you used.
if you don't provide an mxid, it will check your account.

only the account running the bot can specify a room ID.

if the server the bot is running on is missing `m.room.member` events, it will stop at the
event that references a missing event. not as useful, but at least there's a warning.

- `!acldiff` - see the difference between the current room ACL and the ACL before it.

- `!compact <text>` - this is a silly command, has nothing to do with state.
try it out, though! it would convert 'kubernetes' to 'k8s', for example.

## usage
1. install `matrix-nio` for python
2. `cp config.json.example config.json`, put an account in the config
3. `python3 main.py`

# license
agpl v3
