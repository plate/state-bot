import asyncio, json, time, re
from nio import AsyncClient, MatrixRoom, RoomMessageText, RoomGetEventError

with open("config.json", "r") as file:
    config = json.load(file)

def escape(text):
    """Make text suitable for HTML"""
    # TODO: is this truly safe?
    # idea: if this gets too big, use a dict. or drop html entirely
    text = text.replace("<", "&lt;")
    text = text.replace(">", "&gt;")
    return text

async def send(room_id, message, html=None):
    """Send message with the correct msgtype"""
    if html:
        content = {
            "body": message,
            "formatted_body": html,
            "format": "org.matrix.custom.html",
            "msgtype": "m.notice"
        }
    else:
        content = {
            "body": message,
            "msgtype": "m.notice"
        }
    await client.room_send(
        room_id,
        message_type = "m.room.message",
        content = content
    )

async def get_acl_diff(room_id):
    # get the room state
    print("getting room state for", room_id)
    # room_get_state_event won't give us replaces_state,
    # which i need
    room_state = await client.room_get_state(room_id)
    print(room_state)
    # get the ACL event
    current_acl = None
    for event in room_state.events:
        if event['type'] == "m.room.server_acl":
            current_acl = event

    # if there's no ACL, quit
    if not current_acl:
        print("no acl")
        await send(room_id, "can't see an acl here chief")
        return

    # get the previous event
    prev_id = current_acl['replaces_state']
    prev_acl = await client.room_get_event(room_id, prev_id)
    prev_acl = prev_acl.event.source

    # turn deny into sets and compare the two
    deny_diff = set.symmetric_difference(
        set(current_acl['content']['deny']),
        set(prev_acl['content']['deny'])
    )
    # the same, but for allow
    allow_diff = set.symmetric_difference(
        set(current_acl['content']['allow']),
        set(prev_acl['content']['allow'])
    )
    # create a message
    print(allow_diff, deny_diff)
    print(current_acl)
    message = "\n".join([
        f"Allow: {allow_diff} ({len(allow_diff)} server(s) added)",
        f"Deny: {deny_diff} ({len(deny_diff)} server(s) added)",
        f"({prev_id} -> {current_acl['event_id']})"
    ])
    html_message = "<br>".join([
        f"Allow: {allow_diff} ({len(allow_diff)} server(s) added)",
        f"Deny: {deny_diff} ({len(deny_diff)} server(s) added)",
        f"(<code>{prev_id}</code> -> <code>{current_acl['event_id']}</code>)"
    ])
    await send(room_id, message, html_message)

async def get_join_ts(user, room_id, send_room):
    """Find out when a user first joined"""
    # get the room state
    print("getting room state for", room_id)
    # room_get_state_event won't give us replaces_state,
    # which i need - use room_get_state instead
    room_state = await client.room_get_state(room_id)
    avatars = set() # history of avatars the user had
    membership = None # set to the state event we're looking at
    # filter for the m.room.member state event
    for event in room_state.events:
        if event['type'] == "m.room.member" and event['state_key'] == user:
            membership = event
            if membership['content'].get('avatar_url'):
                avatars.add(membership['content']['avatar_url'])

    # if membership is still None, the user wasn't found
    if membership == None:
        await send(send_room, "That user doesn't exist")
        return

    # from there, go back until we find an event *without*
    # replaces_state (meaning it's the first of its kind)
    first_found = False
    counter = 0
    while not first_found:
        replaces_state = membership.get('unsigned').get('replaces_state')
        print(membership, replaces_state)
        if replaces_state:
            # if there's a previous state, fetch it
            # and check on the next loop
            response = await client.room_get_event(room_id, replaces_state)
            # sometimes, we can't get the previous event referenced, so
            # nio gives an error instead.
            # if this happens, quit immediately. thanks lda
            if isinstance(response, RoomGetEventError):
                await send(send_room, f"Error getting {replaces_state}: {response}\nThe date will be inaccurate!")
                first_found = True
                break
            # if there's no error, get the event dictionary
            membership = response.event.source
            if membership['content'].get('avatar_url'):
                avatars.add(membership['content']['avatar_url'])
            counter += 1

        else:
            print("first membership event found")
            print("avatar history:", avatars)
            first_found = True

    # format the time
    time_format = '%Y-%m-%d %H:%M:%S'
    join_seconds = int(membership['origin_server_ts'] / 1000)
    join_date = time.strftime(time_format, time.gmtime(join_seconds))

    # format message
    event_link = f"https://matrix.to/#/{escape(room_id)}/{membership['event_id']}"
    message = '\n'.join([
        f"{escape(user)} joined on {join_date} (event id: {membership['event_id']} / {event_link})",
        f"checked {counter} event(s) to find out"
    ])

    html_message = '<br>'.join([
        f"{user} joined on {join_date} (event id: <a href=\"{event_link}\"><code>{membership['event_id']}</code></a>)",
        f"checked <b>{counter}</b> event(s) to find out",
        "avatars:<br>"
    ])

    for url in avatars:
        html_message += f'<img height="32" src="{escape(url)}" alt="an avatar">,'

    await send(send_room, message, html_message)

def compact(match):
    """Shorten a word in a regex match. Used by compact_message"""
    word = match.group(0)
    # {first char}{length of middle}{last char}
    return f"{word[0]}{len(word[1:-1])}{word[-1]}"

async def compact_message(sentence, room_id):
    """Shorten all words in a message using compact() (like word -> w2d)"""
    # get the actual text
    # if there's nothing to convert, tell the user
    if len(sentence) == 0:
        await send(room_id, "!compact needs text to work with.")
        return
    # don't make this return. someone input 'kys' and it was funny
    if len(sentence) <= 3:
        await send(room_id, "that's a three letter word. there's no point.")
    # use re to replace words with compact()
    new_sentence = re.sub(r'\w{4,}', compact, sentence)
    await send(room_id, new_sentence)

async def on_message(room, event):
    """Trigger the right function based on message content"""
    args = event.body.split(' ')
    cmd = args.pop(0)
    # get the change between the current ACL and the last
    if cmd == "!acldiff":
        await get_acl_diff(room.room_id)

    # make text easy to read
    elif cmd == "!compact":
        sentence = event.body[len('!compact'):]
        await compact_message(event.body, room.room_id)

    # find when user first joined
    elif cmd in ("!firstjoin", "!fartsjoin", "!fuckjohn"):
        user = event.sender
        chosen_room = room.room_id
        # everyone is allowed to specify a user in the room they
        # use the bot from. i'm allowed to choose any room
        if len(args) > 0:
            user = args[0]
        if event.sender in config['admins'] and len(args) > 1:
            chosen_room = args[1]
        print("parameters:", user, chosen_room)
        await get_join_ts(user, chosen_room, room.room_id)
        
async def main():
    await client.sync(timeout=30000)
    print("synced, waiting for messages")
    client.add_event_callback(on_message, RoomMessageText)
    await client.sync_forever(timeout=30000)

client = AsyncClient(config['homeserver'])
client.user_id = config['username']
client.access_token = config['token']

asyncio.get_event_loop().run_until_complete(main())
